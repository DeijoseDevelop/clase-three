#from rest_framework.decorators import api_view
from rest_framework.response import Response
#from django.http import JsonResponse
from .serializers import UserSerializer
from .models import User
#from rest_framework.generics import ListAPIView
#from rest_framework.generics import CreateAPIView
#from rest_framework.generics import DestroyAPIView
from rest_framework.views import APIView
from django.http import Http404
from rest_framework import status


"""
@api_view(['GET'])
def hello(request):
    users = User.objects.all()
    serializer = UserSerializer(users, many=True)
    return JsonResponse(serializer.data, safe=False)
"""
"""
class UserList(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class CreateView(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class DestroyView(DestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
"""

class ListUsers(APIView):
    def get(self, request, format=None):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)

class UserApi(APIView):
    def get_object(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        user = self.get_object(pk)
        serializer_class = UserSerializer(user)
        return Response(serializer_class.data)

    def put(self, request, pk):
        user = User.objects.get(pk=pk)
        serializer_class = UserSerializer(user, data=request.data)
        if serializer_class.is_valid():
            serializer_class.save()
            return Response(serializer_class.data)
        else:
            return Response(serializer_class.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        user = User.objects.get(pk=pk)
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)