#from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from .models import User

"""
class UserSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=100, unique=True)
    password = serializers.CharField(max_length=100)
    email = serializers.EmailField(unique=True)
    firstname = serializers.CharField(max_length=100)
    last_name = serializers.CharField(max_length=100)
    description = serializers.TextField()
    """

class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password', 'email', 'first_name', 'last_name', 'description')