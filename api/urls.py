from django.urls import path
from .views import UserApi ,ListUsers #UserList, CreateView, DestroyView, hello


urlpatterns = [
    #path('', hello, name='hello'),
    #path('get/', UserList.as_view(), name='get'),
    #path('post/', CreateView.as_view(), name='post'),
    #path('delete/<int:pk>/', DestroyView.as_view(), name='delete'),
    path('', ListUsers.as_view(), name='list'),
    path('<int:pk>', UserApi.as_view(), name='user'),
]