from django.db import models


class User(models.Model):
    """
    Model representing a user.
    """
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    email = models.EmailField()
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    description = models.TextField()

    def __str__(self):
        return self.username